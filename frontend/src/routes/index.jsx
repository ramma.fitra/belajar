import React from 'react';
import {Route, Switch} from 'react-router-dom';
import HomePage from '../pages/HomePage';
import ProdukPage from '../pages/ProdukPage';
import ProdukDetailPage from '../pages/ProdukDetailPage';
import CheckoutPage from '../pages/CheckoutPage';
import KontakPage from '../pages/KontakPage';
import TentangPage from '../pages/TentangPage';
import TransaksiPage from '../pages/TransaksiPage';
import KeranjangPage from '../pages/KeranjangPage';
import NavBar from '../components/NavBar';

function Routes() {
  return (
    <div>
      <NavBar/>
      <Switch>
        <Route path="/" exact component={HomePage}/>
        <Route path="/KeranjangPage" exact component={KeranjangPage}/>
        <Route path="/ProdukPage" exact component={ProdukPage}/>
        <Route path="/ProdukDetailPage" exact component={ProdukDetailPage} />
        <Route path="/CheckoutPage" exact component={CheckoutPage} />
        <Route path="/KontakPage" exact component={KontakPage} />
        <Route path="/TentangPage" exact component={TentangPage} />
        <Route path="/TransaksiPage" exact component={TransaksiPage} />
      </Switch>
    </div>
  )
}

export default Routes
