import React from 'react';
import {Link} from 'react-router-dom';

function NavBar() {
  return (
    <div>
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/CheckoutPage">Checkout</Link></li>
        <li><Link to="/KeranjangPage">Keranjang</Link></li>
        <li><Link to="/KontakPage">Contact</Link></li>
        <li><Link to="/ProdukDetailPage">Detail Product</Link></li>
        <li><Link to="/ProdukPage">Product</Link></li>
        <li><Link to="/TentangPage">About US</Link></li>
        <li><Link to="/TransaksiPage">Transaction</Link></li>
      </ul>
    </div>
  )
}

export default NavBar
